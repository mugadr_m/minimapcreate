//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MMCGUI.rc
//
#define IDD_DIALOG1                     101
#define IDC_IMAGE_BACKG                 1001
#define IDC_IMG1                        1002
#define IDC_MPQ_PATH                    1003
#define IDC_OUTPUT_PATH                 1004
#define IDC_BROWSE_OUTPUT               1005
#define IDC_STEP_DESC                   1006
#define IDC_CONTINENT_NAME              1007
#define IDC_BTN_LOAD                    1008
#define IDC_TILE_AVAIL                  1009
#define IDC_TILE_SEL                    1010
#define IDC_SEL_ALL                     1011
#define IDC_DESEL_ALL                   1012
#define IDC_SEL_BTN                     1013
#define IDC_DESEL_BTN                   1014
#define IDC_BUILD_BTN                   1015
#define IDC_PROGRESS1                   1016
#define IDC_BUILD_PROGRESS              1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
