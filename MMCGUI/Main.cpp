#include <Windows.h>
#include <windowsx.h>
#include <iostream>
#include <Shlwapi.h>
#include <ShObjIdl.h>
#include <ShlObj.h>
#include <vector>
#include <list>
#include <sstream>
#include <algorithm>
#include <thread>
#include <mutex>
#include <DbgHelp.h>
#include <gdiplus.h>
#include <atlbase.h>
#include "resource.h"

#include "../MinimapCreate/MinimapCreate.h"

#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "gdiplus.lib")
#pragma comment(lib, "dbghelp.lib")

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

std::shared_ptr<IMinimapCreate> mmcInstance;
HWND hDialog;
FileHandler mpqHandler;
std::vector<std::string> availableTiles;
std::vector<std::string> selectedTiles;
std::list<std::string> loadTiles;
std::mutex loadLock;
int totalTiles = 0;
int curProgress = 0;
bool isRunning = true;

#define WM_INFORM_PROGRESS	(WM_USER + 0x100)

int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	using namespace Gdiplus;

	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	ImageCodecInfo* pImageCodecInfo = NULL;

	GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (ImageCodecInfo*) (malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

void loadThread() {
	mmcInstance = minimapCreateInit();
	mmcInstance->initGraphics();
	
	while (isRunning) {
		{
			std::lock_guard<std::mutex> l(loadLock);
			if (loadTiles.size() > 0) {
				for (auto itr = loadTiles.begin(); itr != loadTiles.end(); ) {
					auto tile = *itr;
					auto old = itr;
					++itr;
					loadTiles.erase(old);

					HBITMAP backg = mmcInstance->fromHandler(tile);
					if(!backg) {
                        ++curProgress;
                        SendMessage(hDialog, WM_INFORM_PROGRESS, 0, 0);
                        continue;
					}

					Gdiplus::GdiplusStartupInput inp;
					ULONG_PTR token;
					Gdiplus::GdiplusStartup(&token, &inp, nullptr);

					Gdiplus::Bitmap* bmp = Gdiplus::Bitmap::FromHBITMAP(backg, nullptr);

					DeleteObject(backg);
					
					char curDir[4096];
					GetWindowText(GetDlgItem(hDialog, IDC_OUTPUT_PATH), curDir, 4096);
					std::stringstream file;
					file << curDir << "\\" << tile;
					char* path = _strdup(file.str().c_str());
					*PathFindExtension(path) = '\0';
					file = std::stringstream();
					file << path << ".png";
					free(path);
					path = _strdup(file.str().c_str());
					*PathFindFileName(path) = '\0';
					SHCreateDirectoryEx(nullptr, path, nullptr);
					free(path);

					std::wstring wstr;
					std::string str = file.str();
					std::copy(str.begin(), str.end(), std::back_inserter(wstr));

					CLSID enc;
					GetEncoderClsid(L"image/png", &enc);
					bmp->Save(wstr.c_str(), &enc);
					delete bmp;
					++curProgress;
					SendMessage(hDialog, WM_INFORM_PROGRESS, 0, 0);
				}
			}
		}

		std::this_thread::sleep_for(std::chrono::milliseconds(30));
	}
}

INT_PTR CALLBACK DlgProcHandler(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_CLOSE:
		EndDialog(hWindow, 0);
		return TRUE;

	case WM_SHOWWINDOW:
		{
			hDialog = hWindow;
			if (wParam == FALSE) {
				break;
			}

			static bool isInitial = true;
			if (isInitial) {
				isInitial = false;

                CComPtr<IFileOpenDialog> dlg{};
                CoCreateInstance(CLSID_FileOpenDialog, nullptr, CLSCTX_ALL, IID_PPV_ARGS(&dlg));
                FILEOPENDIALOGOPTIONS opts{};
                dlg->GetOptions(&opts);
                opts |= FOS_FILEMUSTEXIST | FOS_PICKFOLDERS;
                dlg->SetOptions(opts);
                dlg->SetTitle(L"Please select World of Warcraft (3.3.5a) data folder");

                if(dlg->Show(nullptr) == HRESULT_FROM_WIN32(ERROR_CANCELLED)) {
                    MessageBox(nullptr, "No path selected, exiting", "Error", MB_OK);
                    ExitProcess(1);
                }

                CComPtr<IShellItem> res_item{};
                dlg->GetResult(&res_item);

                LPWSTR fs_path{};
                res_item->GetDisplayName(SIGDN_FILESYSPATH, &fs_path);
                std::wstring folder = fs_path;
                CoTaskMemFree(fs_path);

                std::string path = utf16ToUtf8(folder);
				mpqHandler = minimapCreateNewMpqHandler(path);
				mmcInstance->initFileSystem(mpqHandler);
				SetWindowText(GetDlgItem(hWindow, IDC_MPQ_PATH), path.c_str());

				char cur_dir[MAX_PATH]{};
				GetCurrentDirectory(MAX_PATH, cur_dir);
				SetWindowText(GetDlgItem(hWindow, IDC_OUTPUT_PATH), cur_dir);
			}
		}
		return TRUE;

	case WM_COMMAND:
		{
			switch (LOWORD(wParam)) {
			case IDC_BROWSE_OUTPUT:
				{
					if (HIWORD(wParam) == BN_CLICKED) {
						BROWSEINFO bi = { 0 };
						bi.hwndOwner = hWindow;
						bi.lpszTitle = "Please select the folder to output images to";
						bi.ulFlags = BIF_EDITBOX | BIF_VALIDATE | BIF_NEWDIALOGSTYLE;

						LPITEMIDLIST idl = SHBrowseForFolder(&bi);
						char path[MAX_PATH + 1] = { 0 };
						SHGetPathFromIDList(idl, path);
						SetWindowText(GetDlgItem(hWindow, IDC_OUTPUT_PATH), path);
					}
				}
				break;

			case IDC_BTN_LOAD:
				{
					char continent[4096];
					GetDlgItemText(hWindow, IDC_CONTINENT_NAME, continent, 4096);

					if (strlen(continent) == 0) {
						break;
					}

					ListBox_ResetContent(GetDlgItem(hWindow, IDC_TILE_SEL));
					ListBox_ResetContent(GetDlgItem(hWindow, IDC_TILE_AVAIL));

					std::string baseName = "World\\Maps\\";
					baseName += continent;
					baseName += "\\";

					for (int i = 0; i < 64; ++i) {
						for (int j = 0; j < 64; ++j) {
							std::stringstream strm;
							strm << baseName << continent << "_" << i << "_" << j << ".adt";
							if (mpqHandler->hasFile(strm.str().c_str())) {
								std::stringstream adt;
								adt << continent << "_" << i << "_" << j << ".adt";
								ListBox_AddString(GetDlgItem(hWindow, IDC_TILE_AVAIL), adt.str().c_str());
								EnableWindow(GetDlgItem(hWindow, IDC_SEL_ALL), TRUE);
								EnableWindow(GetDlgItem(hWindow, IDC_SEL_BTN), TRUE);
								availableTiles.push_back(adt.str().c_str());
							}
						}
					}
				}
				break;

			case IDC_SEL_ALL:
				{
					for (auto& tile : availableTiles) {
						selectedTiles.push_back(tile);
						ListBox_AddString(GetDlgItem(hWindow, IDC_TILE_SEL), tile.c_str());
						EnableWindow(GetDlgItem(hWindow, IDC_DESEL_ALL), TRUE);
						EnableWindow(GetDlgItem(hWindow, IDC_DESEL_BTN), TRUE);
					}

					ListBox_ResetContent(GetDlgItem(hWindow, IDC_TILE_AVAIL));
					availableTiles.clear();
					EnableWindow(GetDlgItem(hWindow, IDC_SEL_ALL), FALSE);
					EnableWindow(GetDlgItem(hWindow, IDC_SEL_BTN), FALSE);
					EnableWindow(GetDlgItem(hWindow, IDC_BUILD_BTN), TRUE);
				}
				break;

			case IDC_DESEL_ALL:
				{
					for (auto& tile : selectedTiles) {
						availableTiles.push_back(tile);
						ListBox_AddString(GetDlgItem(hWindow, IDC_TILE_AVAIL), tile.c_str());
						EnableWindow(GetDlgItem(hWindow, IDC_SEL_ALL), TRUE);
						EnableWindow(GetDlgItem(hWindow, IDC_SEL_BTN), TRUE);
					}

					ListBox_ResetContent(GetDlgItem(hWindow, IDC_TILE_SEL));
					selectedTiles.clear();
					EnableWindow(GetDlgItem(hWindow, IDC_DESEL_ALL), FALSE);
					EnableWindow(GetDlgItem(hWindow, IDC_DESEL_BTN), FALSE);
					EnableWindow(GetDlgItem(hWindow, IDC_BUILD_BTN), FALSE);
				}
				break;

			case IDC_SEL_BTN:
				{
					auto wnd = GetDlgItem(hWindow, IDC_TILE_AVAIL);
					unsigned int numSel = ListBox_GetSelCount(wnd);
					std::vector<int> sels(numSel);
					ListBox_GetSelItems(wnd, numSel, sels.data());

					for (unsigned int i : sels) {
						if (i < 0 || i >= availableTiles.size()) {
							continue;
						}

						std::string str = availableTiles[i];
						selectedTiles.push_back(str);
						ListBox_AddString(GetDlgItem(hWindow, IDC_TILE_SEL), str.c_str());
					}

					std::sort(sels.begin(), sels.end());
					for (unsigned int i = 0; i < sels.size(); ++i) {
						availableTiles.erase(availableTiles.begin() + (sels[i] - i));
						ListBox_DeleteString(wnd, sels[i] - i);
					}

					EnableWindow(GetDlgItem(hWindow, IDC_SEL_ALL), availableTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_SEL_BTN), availableTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_BUILD_BTN), selectedTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_DESEL_ALL), selectedTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_DESEL_BTN), selectedTiles.size() > 0);
				}
				break;

			case IDC_DESEL_BTN:
				{
					auto wnd = GetDlgItem(hWindow, IDC_TILE_SEL);
					unsigned int numSel = ListBox_GetSelCount(wnd);
					std::vector<int> sels(numSel);
					ListBox_GetSelItems(wnd, numSel, sels.data());

					for (unsigned int i : sels) {
						if (i < 0 || i >= selectedTiles.size()) {
							continue;
						}

						std::string str = selectedTiles[i];
						availableTiles.push_back(str);
						ListBox_AddString(GetDlgItem(hWindow, IDC_TILE_AVAIL), str.c_str());
					}

					std::sort(sels.begin(), sels.end());
					for (unsigned int i = 0; i < sels.size(); ++i) {
						selectedTiles.erase(selectedTiles.begin() + (sels[i] - i));
						ListBox_DeleteString(wnd, sels[i] - i);
					}

					EnableWindow(GetDlgItem(hWindow, IDC_SEL_ALL), availableTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_SEL_BTN), availableTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_BUILD_BTN), selectedTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_DESEL_ALL), selectedTiles.size() > 0);
					EnableWindow(GetDlgItem(hWindow, IDC_DESEL_BTN), selectedTiles.size() > 0);
				}
				break;

			case IDC_BUILD_BTN:
				{
					std::lock_guard<std::mutex> l(loadLock);
					loadTiles.clear();
					auto wnd = GetDlgItem(hWindow, IDC_TILE_SEL);
					int numItems = ListBox_GetCount(wnd);
					for (int i = 0; i < numItems; ++i) {
						int len = ListBox_GetTextLen(wnd, i);
						std::vector<char> curStr(len + 1);
						ListBox_GetText(wnd, i, curStr.data());
						char continent[4096];
						GetDlgItemText(hWindow, IDC_CONTINENT_NAME, continent, 4096);

						std::stringstream strm;
						strm << "World\\Maps\\" << continent << "\\" << curStr.data();
						loadTiles.push_back(strm.str());
					}

					curProgress = 0;
					totalTiles = loadTiles.size();

					ShowWindow(GetDlgItem(hWindow, IDC_BUILD_BTN), SW_HIDE);
					ShowWindow(GetDlgItem(hWindow, IDC_BUILD_PROGRESS), SW_SHOW);

					SendDlgItemMessage(hWindow, IDC_BUILD_PROGRESS, PBM_SETRANGE, 0, (100 << 16));
					SendDlgItemMessage(hWindow, IDC_BUILD_PROGRESS, PBM_SETPOS, 0, 0);
				}
				break;
			}
		}
		return TRUE;

	case WM_INFORM_PROGRESS:
		{
			float pct = (float) curProgress / (float) totalTiles;
			int value = (int)min(std::floor(pct * 100), 100);
			SendDlgItemMessage(hWindow, IDC_BUILD_PROGRESS, PBM_SETPOS, value, 0);
			if (curProgress == totalTiles) {
				ShowWindow(GetDlgItem(hWindow, IDC_BUILD_BTN), SW_SHOW);
				ShowWindow(GetDlgItem(hWindow, IDC_BUILD_PROGRESS), SW_HIDE);
			}
		}
		break;

	case WM_INITDIALOG:
		return TRUE;
	}

	return FALSE;
}

LONG WINAPI ExceptionFilter(LPEXCEPTION_POINTERS info) {
	auto cur = time(nullptr);
	tm tmsf;
	tm* tm = &tmsf;
	localtime_s(tm, &cur);
	char fileName[MAX_PATH];
	_snprintf_s(fileName, MAX_PATH, "MinimapDump_%02u_%02u_%04u_%02u_%02u_%02u.dmp", tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900, tm->tm_hour, tm->tm_min, tm->tm_sec);
	auto file = CreateFile(fileName, FILE_ALL_ACCESS, 0, nullptr, CREATE_ALWAYS, 0, nullptr);
	MINIDUMP_EXCEPTION_INFORMATION exInfo;
	exInfo.ExceptionPointers = info;
	exInfo.ClientPointers = TRUE;
	exInfo.ThreadId = GetCurrentThreadId();

	MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), file, MiniDumpWithIndirectlyReferencedMemory, &exInfo, nullptr, nullptr);
	CloseHandle(file);

	std::stringstream strm;
	strm << "There was an error executing the program. Information is stored in the file: " << fileName;
	MessageBox(hDialog, strm.str().c_str(), "Error!", MB_OK | MB_ICONEXCLAMATION);

	ExitProcess(0);
}

BOOL WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, INT) {
	if (IsDebuggerPresent() == FALSE) {
		SetUnhandledExceptionFilter(ExceptionFilter);
	}

	CoInitialize(nullptr);

	std::thread workThread(loadThread);

	DialogBox(GetModuleHandle(nullptr), MAKEINTRESOURCE(IDD_DIALOG1), GetDesktopWindow(), &DlgProcHandler);

	isRunning = false;
	workThread.join();

}