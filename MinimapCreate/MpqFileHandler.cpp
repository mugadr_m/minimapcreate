#include "stdafx.h"
#include "MpqFileHandler.h"

MpqFileHandler::MpqFileHandler(const std::string& basePath) {
	using namespace std::tr2::sys;

	path base(basePath);

	for (directory_iterator itr(base); itr != directory_iterator(); ++itr) {
		auto curFile = itr->path();
		auto ext = curFile.extension().string();
		std::transform(ext.begin(), ext.end(), ext.begin(), tolower);
		if (ext != ".mpq") {
			continue;
		}

		HANDLE hArchive = nullptr;
		std::string path = curFile.string();
		auto res = SFileOpenArchive(path.c_str(), 0, 1, &hArchive);
		
		if (res == false) {
			continue;
		}

		auto entry = ArchiveEntry();
		entry.path = path;
		entry.archive = hArchive;

		mArchives.push_back(entry);
	}

	mArchives.sort(
		[](ArchiveEntry& e1, ArchiveEntry& e2) {
			std::wstring a = utf8ToUtf16(path(e1.path).filename().string());
			std::wstring b = utf8ToUtf16(path(e2.path).filename().string());

			std::transform(a.begin(), a.end(), a.begin(), towlower);
			std::transform(b.begin(), b.end(), b.begin(), towlower);

			std::wstring::size_type pi1 = a.find(L"patch");
			std::wstring::size_type pi2 = b.find(L"patch");

			bool hasPatchA = pi1 != std::wstring::npos;
			bool hasPatchB = pi2 != std::wstring::npos;

			if (hasPatchA && !hasPatchB)
				return true;

			if (!hasPatchA && hasPatchB)
				return false;

			if (hasPatchA && hasPatchB) {
				bool hasIdentA = a[5] == '-';
				bool hasIdentB = b[5] == '-';

				if (!hasIdentA && hasIdentB) {
					return false;
				}

				if (hasIdentA && !hasIdentB) {
					return true;
				}

				std::wstring patchIdentA = L"";
				std::wstring patchIdentB = L"";
				if (a.length() > 6)
					patchIdentA = a.substr(6);
				if (b.length() > 6)
					patchIdentB = b.substr(6);

				return patchIdentA.compare(patchIdentB) > 0;
			}

			return a.compare(b) > -1;
		}
	);
}

FilePtr MpqFileHandler::openFile(const std::string& fileName) {
	for (auto& itr : mArchives) {
		HANDLE hFile = nullptr;
		auto res = SFileOpenFileEx(itr.archive, fileName.c_str(), 0, &hFile);
		if (res == TRUE) {
			return std::make_shared<MpqFile>(hFile);
		}
	}

	return nullptr;
}

bool MpqFileHandler::hasFile(const std::string& fileName) {
	for (auto& itr : mArchives) {
		if (SFileHasFile(itr.archive, const_cast<char*>(fileName.c_str())))
			return true;
	}

	return false;
}