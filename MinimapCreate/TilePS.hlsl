varying vec3 vertexNormal;
varying vec3 vertexPosition;
varying vec2 textureCoordinate0;
varying vec2 texCoordAlpha;

uniform sampler2D _texture0;
uniform sampler2D _texture1;
uniform sampler2D _texture2;
uniform sampler2D _texture3;
uniform sampler2D _texAlpha;

const vec3 sunDirection = vec3(1, 1, -1);

vec3 getDiffuseLight(vec3 normal) {
	float light = dot(normal, -normalize(sunDirection));
	if (light < 0.0)
		light = 0.0;
	if (light > 0.5)
		light = 0.5 + (light - 0.5) * 0.65;

	vec3 diffuse = vec3(1, 1, 1) * light;
	diffuse += vec3(0.4, 0.4, 0.4);
	diffuse = clamp(diffuse, 0.0, 1.0);

	return diffuse;
}

#ifdef _LAYER1
void main() {
	vec3 light = getDiffuseLight(vertexNormal);
	vec4 base = texture(_texture0, textureCoordinate0);
	base.rgb *= light.rgb;
	gl_FragColor = base;
}
#endif

#ifdef _LAYER2
void main() {
	vec3 light = getDiffuseLight(vertexNormal);
	vec4 tex0 = texture(_texture0, textureCoordinate0);
	vec4 tex1 = texture(_texture1, textureCoordinate0);
	vec4 dist = texture(_texAlpha, texCoordAlpha);

	vec4 base = dist.g * tex1 + (1.0 - dist.g) * tex0;
	base.rgb *= light.rgb;
	gl_FragColor = base;
}
#endif

#ifdef _LAYER3
void main() {
	vec3 light = getDiffuseLight(vertexNormal);
	vec4 tex0 = texture(_texture0, textureCoordinate0);
	vec4 tex1 = texture(_texture1, textureCoordinate0);
	vec4 tex2 = texture(_texture2, textureCoordinate0);
	vec4 dist = texture(_texAlpha, texCoordAlpha);

	vec4 base = tex0;
	base = dist.g * tex1 + (1.0 - dist.g) * base;
	base = dist.b * tex2 + (1.0 - dist.b) * base;

	base.rgb *= light.rgb;
	gl_FragColor = base;
}
#endif

#ifdef _LAYER4
void main() {
	vec3 light = getDiffuseLight(vertexNormal);
	vec4 tex0 = texture(_texture0, textureCoordinate0);
	vec4 tex1 = texture(_texture1, textureCoordinate0);
	vec4 tex2 = texture(_texture2, textureCoordinate0);
	vec4 tex3 = texture(_texture3, textureCoordinate0);
	vec4 dist = texture(_texAlpha, texCoordAlpha);

	vec4 base = tex0;
	base = dist.g * tex1 + (1.0 - dist.g) * base;
	base = dist.b * tex2 + (1.0 - dist.b) * base;
	base = dist.a * tex3 + (1.0 - dist.a) * base;

	base.rgb *= light.rgb;
	gl_FragColor = base;
}
#endif