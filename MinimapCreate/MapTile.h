#pragma once

#include "IFile.h"
#include "IFileHandler.h"
#include "BlpTexture.h"

class GxDevice;
typedef std::shared_ptr<GxDevice> GxDevicePtr;

struct MCIN
{
	unsigned int mcnk;
	unsigned int size;
	unsigned int flags;
	unsigned int unused;
};

struct MCNK
{
	unsigned int Flags;
	unsigned int IndexX;
	unsigned int IndexY;
	unsigned int NumLayers;
	unsigned int NumDoodads;
	unsigned int mcvt;
	unsigned int mcnr;
	unsigned int mcly;
	unsigned int mcrf;
	unsigned int mcal;
	unsigned int sizeAlpha;
	unsigned int mcsh;
	unsigned int sizeShadow;
	unsigned int AreaId;
	unsigned int NumWmo;
	unsigned int holeBitmap;

	unsigned __int64 effectMap[2];

	unsigned int predTex;
	unsigned int noEffectDoodad;
	unsigned int mcse;
	unsigned int numSoundEmitters;
	unsigned int mclq;
	unsigned int sizeLiquid;
	float posx, posy, posz;
};

struct MCLY
{
	unsigned int texture;
	unsigned int flags;
	unsigned int ofsMCAL;
	signed short effect;
	unsigned short padding;
};

struct Vertex
{
	float x, y, z;
	float nx, ny, nz;
	float U, V;
	float S, T;
};

class MapTile;
class GxDevice;

class MapChunk
{
private:
	struct Chunk
	{
		unsigned int signature;
		unsigned int size;
		unsigned int dataPos;
	};

	FilePtr mFile;
	MapTile* mParent;
	MCIN mInfo;
	MCNK mHeader;
	std::map<unsigned int, Chunk> mChunks;
	std::vector<Vertex> mVertices;
	std::vector<unsigned int> mAlphaData;
	std::vector<MCLY> mLayers;
	bool mLoaded = false;

	GLuint mAlphaTex;
	GLuint mVertexBuffer;
	GLuint mIndexBuffer;

	void loadChunk(unsigned int offset, uint32_t suggested_size = 0);
	void loadVertices();
	void loadLayers();
public:
	MapChunk(MCIN info, FilePtr file, MapTile* parent);
	~MapChunk();

	void loadHeader();
	void render(GxDevice* dev);
	float getMaxHeight() {
		float curMax = -FLT_MAX;
		for (auto& v : mVertices) {
			if (v.z > curMax) {
				curMax = v.z;
			}
		}

		return curMax;
	}

	static void initialize(GxDevice* dev);
};

class MapTile
{
private:
	struct Chunk
	{
		unsigned int signature;
		unsigned int size;
		unsigned int dataPos;
	};

	std::map<unsigned int, Chunk> mChunks;
	std::vector<std::shared_ptr<BlpTexture>> mTextures;
	std::vector<MCIN> mMcin;
	std::vector<std::shared_ptr<MapChunk>> mMapChunks;
	FilePtr mFile;
	FileHandler mHandler;
	GxDevicePtr mDevice;

	void loadTextures();
	void loadMcin();
	void loadChunks();
public:
	MapTile(FilePtr file, FileHandler handler, GxDevicePtr device);
	~MapTile();

	GLuint getTexture(uint32_t index) {
		return mTextures[index]->getTexture();
	}

	void render(GxDevice* dev);

	float getMaxHeight() {
		float curMax = -FLT_MAX;
		for (auto& cnk : mMapChunks) {
			if (cnk->getMaxHeight() > curMax) {
				curMax = cnk->getMaxHeight();
			}
		}

		return curMax;
	}
};