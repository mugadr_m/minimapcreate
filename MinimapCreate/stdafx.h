#pragma once

#include <Windows.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <memory>
#include <list>
#include <filesystem>
#include <algorithm>
#include <codecvt>
#include <map>
#include <gdiplus.h>
#include <sstream>

#include <gl/GL.h>
#include <gl/GLU.h>
#include "gl/glext.h"
#include "gl/wglext.h"
#include "GlExt.h"

#ifdef _UNICODE
#define _UNICODE_RESET
#undef _UNICODE
#endif

#include "StormLib.h"

#ifdef _UNICODE_RESET
#define _UNICODE
#endif

extern HMODULE gDllModule;

std::wstring utf8ToUtf16(const std::string& str);