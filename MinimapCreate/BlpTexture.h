#pragma once

#include "IFile.h"

class GxDevice;

class BlpTexture
{
private:
	struct BlpHeader
	{
		unsigned int Magic;
		unsigned int Version;
		unsigned char Compression;
		unsigned char AlphaDepth;
		unsigned char AlphaCompression;
		unsigned char MipLevels;
		unsigned int width;
		unsigned int height;
		unsigned int Offsets[16];
		unsigned int Sizes[16];

	} mHeader;

	GLuint mTexture;

	void parse(FilePtr file, GxDevice* dev);
	void parse_palette(FilePtr file);
public:
	BlpTexture(FilePtr file, GxDevice* dev);
	~BlpTexture();

	GLuint getTexture() const { return mTexture; }
};