#pragma once

#include "Matrix.h"

class MapTile;

class GxDevice
{
	HWND mWindow;
	HGLRC mContext;
	HDC mWindowDC;
	GLuint mRenderTexture;
	GLuint mFrameBuffer;
	GLuint mPrograms[4];
	Matrix mView, mProj;

	std::string getResourceShader(const std::string& resName) const;

	static LRESULT CALLBACK WndProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam);
	LRESULT onMessage(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam);

	void initWindow();
	void initDevice();
	void initShaders();

	GLuint createProgram(const std::string& codeVS, const std::string& codeFS, std::initializer_list<std::string> macros);
public:
	GxDevice();

	HBITMAP render(MapTile* tile);
	void setLayerShader(unsigned int numLayers);
	GLuint getLayerShader(unsigned int numLayers) {
		assert(numLayers <= 4);
		return mPrograms[numLayers ? numLayers - 1 : 0];
	}
};

typedef std::shared_ptr<GxDevice> GxDevicePtr;