#include "stdafx.h"
#include "GxDevice.h"
#include "MapTile.h"
#include "GlExt.h"

#define BITMAP_SIZE 256

void loadExtensions();

GxDevice::GxDevice() {
	initWindow();
	initDevice();
	initShaders();
}

void GxDevice::initWindow() {
	WNDCLASS wc = { 0 };
	wc.hInstance = GetModuleHandle(nullptr);
	wc.lpszClassName = "__internal_GxWindowClass";
	wc.lpfnWndProc = WndProc;

	RegisterClass(&wc);

	mWindow = CreateWindow("__internal_GxWindowClass", "hidde", WS_OVERLAPPEDWINDOW, 0, 0, 1, 1, nullptr, nullptr, wc.hInstance, this);
	mWindowDC = GetDC(mWindow);
}

void GxDevice::initDevice() {
	PIXELFORMATDESCRIPTOR pfd = { 0 };
	pfd.nSize = sizeof(pfd);
	pfd.cColorBits = 24;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 16;
	pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;

	int format = ChoosePixelFormat(mWindowDC, &pfd);
	SetPixelFormat(mWindowDC, format, &pfd);

	mContext = wglCreateContext(mWindowDC);
	wglMakeCurrent(mWindowDC, mContext);

	loadExtensions();

	glGenFramebuffers(1, &mFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, mFrameBuffer);

	glGenTextures(1, &mRenderTexture);
	glBindTexture(GL_TEXTURE_2D, mRenderTexture);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, BITMAP_SIZE, BITMAP_SIZE, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	GLuint depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, BITMAP_SIZE, BITMAP_SIZE);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, mRenderTexture, 0);
	GLenum drawBuffers [] = { GL_COLOR_ATTACHMENT0 };

	glDrawBuffers(1, drawBuffers);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	mView = Matrix::lookAt(Vector3((533 + 1.0f / 3.0f) / 2.0f, (533 + 1.0f / 3.0f) / 2.0f, 600.0f), Vector3((533 + 1.0f / 3.0f) / 2.0f, (533 + 1.0f / 3.0f) / 2.0f, 0.0f), Vector3(-1, 0, 0));
	mProj = Matrix::ortho(-(533.0f + 1.0f / 3.0f) / 2.0f, -(533.0f + 1.0f / 3.0f) / 2.0f, (533.0f + 1.0f / 3.0f) / 2.0f, (533.0f + 1.0f / 3.0f) / 2.0f, 5.0f, 10000.0f);

	MapChunk::initialize(this);
}

GLuint GxDevice::createProgram(const std::string& codeVS, const std::string& codeFS, std::initializer_list<std::string> macros) {
	std::stringstream strm;
	for (auto& macro : macros) {
		strm << "#define " << macro << "\r\n";
	}

	strm << "\r\n";
	strm << codeFS;

	GLuint vs, fs;
	vs = glCreateShader(GL_VERTEX_SHADER);
	fs = glCreateShader(GL_FRAGMENT_SHADER);

	const char* cvs = codeVS.c_str();
	char* cfs = _strdup(strm.str().c_str());

	int len = strlen(cvs);
	glShaderSource(vs, 1, &cvs, &len);
	len = strlen(cfs);
	glShaderSource(fs, 1, &cfs, &len);
	free(cfs);

	glCompileShader(vs);
	glCompileShader(fs);

	int svs, sfs;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &svs);
	glGetShaderiv(fs, GL_COMPILE_STATUS, &sfs);

	assert(svs == TRUE && sfs == TRUE);

	GLuint prog = glCreateProgram();
	glAttachShader(prog, vs);
	glAttachShader(prog, fs);

	glLinkProgram(prog);

	int sprog;
	glGetProgramiv(prog, GL_LINK_STATUS, &sprog);
	assert(sprog == TRUE);

	return prog;
}

void GxDevice::initShaders() {
	auto codeVS = getResourceShader("TileVS");
	auto codePS = getResourceShader("TilePS");

	mPrograms[0] = createProgram(codeVS, codePS, { "_LAYER1" });
	mPrograms[1] = createProgram(codeVS, codePS, { "_LAYER2" });
	mPrograms[2] = createProgram(codeVS, codePS, { "_LAYER3" });
	mPrograms[3] = createProgram(codeVS, codePS, { "_LAYER4" });

	for (int i = 0; i < 4; ++i) {
		int matUniform = glGetUniformLocation(mPrograms[i], "matView");
		glUseProgram(mPrograms[i]);
		glUniformMatrix4fv(matUniform, 1, true, mView);
		matUniform = glGetUniformLocation(mPrograms[i], "matProj");
		glUniformMatrix4fv(matUniform, 1, true, mProj);
		glUseProgram(0);
	}
}

std::string GxDevice::getResourceShader(const std::string& name) const {
	HMODULE mod = gDllModule;

	HRSRC res = FindResource(mod, name.c_str(), "HLSL");
	if (res == nullptr) {
		throw std::exception("Missing shader");
	}

	HGLOBAL hGlob = LoadResource(mod, res);
	int size = SizeofResource(mod, res);

	std::vector<char> data(size + 1);
	memcpy(data.data(), LockResource(hGlob), size);
	return data.data();
}

HBITMAP GxDevice::render(MapTile* tile) {
	float maxHeight = tile->getMaxHeight();
	mView = Matrix::lookAt(Vector3((533 + 1.0f / 3.0f) / 2.0f, (533 + 1.0f / 3.0f) / 2.0f, maxHeight + 10), Vector3((533 + 1.0f / 3.0f) / 2.0f, (533 + 1.0f / 3.0f) / 2.0f, maxHeight + 9), Vector3(-1, 0, 0));
	for (int i = 0; i < 4; ++i) {
		int matUniform = glGetUniformLocation(mPrograms[i], "matView");
		glUseProgram(mPrograms[i]);
		glUniformMatrix4fv(matUniform, 1, true, mView);
		glUseProgram(0);
	}

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	glBindFramebuffer(GL_FRAMEBUFFER, mFrameBuffer);
	glViewport(0, 0, BITMAP_SIZE, BITMAP_SIZE);

	tile->render(this);

	std::vector<unsigned int> pixels(BITMAP_SIZE * BITMAP_SIZE);
	glReadPixels(0, 0, BITMAP_SIZE, BITMAP_SIZE, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
	for (int i = 0; i < BITMAP_SIZE * BITMAP_SIZE; ++i) {
		auto cur = pixels[i];
		auto r = cur & 0xFF;
		auto g = (cur >> 8) & 0xFF;
		auto b = (cur >> 16) & 0xFF;
		auto a = (cur >> 24) & 0xFF;

		pixels[i] = (a << 24) | (r << 16) | (g << 8) | (b << 0);
	}
	
	return CreateBitmap(BITMAP_SIZE, BITMAP_SIZE, 1, 32, pixels.data());
}

void GxDevice::setLayerShader(unsigned int numLayers) {
	assert(numLayers <= 4);
	glUseProgram(mPrograms[numLayers ? numLayers - 1 : 0]);
}

LRESULT WINAPI GxDevice::WndProc(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	GxDevice* dev = (GxDevice*) GetProp(hWindow, "PROP_DEV_INSTANCE");
	if (dev != nullptr) {
		return dev->onMessage(hWindow, uMsg, wParam, lParam);
	}

	if (uMsg == WM_CREATE) {
		dev = (GxDevice*) ((LPCREATESTRUCT) lParam)->lpCreateParams;
		dev->mWindow = hWindow;
		SetProp(hWindow, "PROP_DEV_INSTANCE", (HANDLE) dev);
		return dev->onMessage(hWindow, uMsg, wParam, lParam);
	}

	return DefWindowProc(hWindow, uMsg, wParam, lParam);
}

LRESULT GxDevice::onMessage(HWND hWindow, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	return DefWindowProc(hWindow, uMsg, wParam, lParam);
}

#define PROC(name) name = (decltype(name))wglGetProcAddress(#name)

void loadExtensions() {
	PROC(glActiveTexture); // GL_ARB_texture_compression
	PROC(glCompressedTexImage2D); // GL_ARB_texture_compression
	PROC(glDebugMessageCallbackAMD); // dynamic
	PROC(glDebugMessageEnableAMD); // dynamic
	PROC(glDebugMessageCallbackARB); // dynamic
	PROC(glDebugMessageControlARB); // dynamic
	PROC(glBindBuffer); // GL_ARB_vertex_buffer_object
	PROC(glBufferData); // GL_ARB_vertex_buffer_object
	PROC(glGenBuffers); // GL_ARB_vertex_buffer_object
	PROC(glDeleteBuffers); // GL_ARB_vertex_buffer_object
	PROC(glGetAttribLocation); // GL_ARB_vertex_shader
	PROC(glVertexAttrib3f); // GL_ARB_vertex_shader
	PROC(glVertexAttrib3fv); // GL_ARB_vertex_shader
	PROC(glUniform1f); // GL_ARB_shader_objects
	PROC(glUniform3f); // GL_ARB_shader_objects
	PROC(glUniform3fv); // GL_ARB_shader_objects
	PROC(glUniform2fv); // GL_ARB_shader_objects
	PROC(glUniformMatrix4fv); // GL_ARB_shader_objects
	PROC(glCreateProgram);  // GL_ARB_shader_objects
	PROC(glShaderSource); // GL_ARB_shader_objects
	PROC(glCreateShader); // GL_ARB_shader_objects
	PROC(glLinkProgram); // GL_ARB_shader_objects
	PROC(glAttachShader); // GL_ARB_shader_objects
	PROC(glGetShaderInfoLog); // GL_ARB_shader_objects
	PROC(glGetProgramInfoLog); // GL_ARB_shader_objects
	PROC(glGetShaderiv); // GL_ARB_shader_objects
	PROC(glGetProgramiv); // GL_ARB_vertex_program
	PROC(glUseProgram); // GL_ARB_shader_objects
	PROC(glCompileShader); // GL_ARB_shader_objects
	PROC(glBindAttribLocation); // GL_ARB_vertex_shader
	PROC(glVertexAttribPointer); // GL_ARB_vertex_shader
	PROC(glGetUniformLocation); // GL_ARB_shader_objects
	PROC(glUniform1i); // GL_ARB_shader_objects
	PROC(glEnableVertexAttribArray); // GL_ARB_vertex_shader
	PROC(glDisableVertexAttribArray); // GL_ARB_vertex_shader
	PROC(glGenerateMipmap); // GL_SGIS_generate_mipmap
	PROC(glVertexAttribDivisor);
	PROC(glGetStringi);
	PROC(glTexBuffer);
	PROC(glDrawElementsInstanced);
	PROC(glFramebufferRenderbuffer);
	PROC(glBindFramebuffer);
	PROC(glBindRenderbuffer);
	PROC(glFramebufferTexture);
	PROC(glGenRenderbuffers);
	PROC(glGenFramebuffers);
	PROC(glRenderbufferStorage);
	PROC(glDrawBuffers);
	PROC(glCheckFramebufferStatus);
}