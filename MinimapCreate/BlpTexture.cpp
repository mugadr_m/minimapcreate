#include "stdafx.h"
#include "BlpTexture.h"
#include "GxDevice.h"

BlpTexture::BlpTexture(FilePtr file, GxDevice* dev) {
	parse(file, dev);
}

BlpTexture::~BlpTexture() {
	glDeleteTextures(1, &mTexture);
	glFlush();
}

void BlpTexture::parse(FilePtr file, GxDevice* dev) {
	if (file == nullptr) {
		return;
	}

	file->setPosition(0);
	file->read(&mHeader, sizeof(BlpHeader));

	int numLevels = 0;
	for (int i = 0; i < 16; ++i) {
		if (mHeader.Offsets[i] != 0 && mHeader.Sizes[i] != 0) {
			++numLevels;
		}
	}

	unsigned int format = 0;
	unsigned int blockSize = 0;

	if (mHeader.Compression == 2) {
		switch (mHeader.AlphaCompression) {
		case 0:
			format = GL_COMPRESSED_RGB_S3TC_DXT1_EXT;
			blockSize = 8;
			break;

		case 1:
			format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
			blockSize = 16;
			break;

		case 7:
			format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
			blockSize = 16;
			break;
		}
	} else if (mHeader.Compression == 3) {
		format = GL_RGBA;
	} else if(mHeader.Compression == 1) {
        parse_palette(file);
        return;
	}

	if (format == 0) {
		throw std::exception("Unsupported texture type");
	}

	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);

	unsigned int curLevel = 0;
	for (unsigned int i = 0; i < 16; ++i) {
		if (mHeader.Sizes[i] > 0 && mHeader.Offsets[i] > 0) {
			file->setPosition(mHeader.Offsets[i]);
			std::vector<unsigned char> data(mHeader.Sizes[i]);
			file->read(data.data(), data.size());

			unsigned int w = std::max<unsigned int>(mHeader.width >> curLevel, 1);
			unsigned int h = std::max<unsigned int>(mHeader.height >> curLevel, 1);
			if(format != GL_RGBA) {
                unsigned int size = ((w + 3) / 4) * ((h + 3) / 4) * blockSize;

                glCompressedTexImage2D(GL_TEXTURE_2D, curLevel, format, max(1, mHeader.width >> curLevel), max(1, mHeader.height >> curLevel), 0, size, data.data());
            } else {
			    glTexImage2D(GL_TEXTURE_2D, curLevel, format, w, h, 0, format, GL_UNSIGNED_BYTE, data.data());
			}
			++curLevel;
		}
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);

	file->close();
}

void BlpTexture::parse_palette(FilePtr file) {
    static const uint32_t ALPHA_LOOKUP1[] = { 0x00, 0xFF };
    static const uint32_t ALPHA_LOOKUP4[] = {
            0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB,
            0xCC, 0xDD, 0xEE, 0xFF
    };

    file->setPosition(sizeof(BlpHeader));

    uint32_t palette[256]{};
    file->read(palette, sizeof palette);

    glGenTextures(1, &mTexture);
    glBindTexture(GL_TEXTURE_2D, mTexture);

    for(auto i = 0u; i < 16u; ++i) {
        if(!mHeader.Sizes[i] || !mHeader.Offsets[i]) {
            break;
        }

        file->setPosition(mHeader.Offsets[i]);
        std::vector<uint8_t> indices(mHeader.Sizes[i]);
        file->read(indices.data(), indices.size());

        unsigned int w = std::max<unsigned int>(mHeader.width >> i, 1);
        unsigned int h = std::max<unsigned int>(mHeader.height >> i, 1);

        std::vector<uint32_t> layer_data(w * h);
        const auto num_entries = w * h;
        for(auto k = 0u; k < num_entries; ++k) {
            const auto index = indices[k];
            auto color = palette[index] | 0xFF000000u;
            layer_data[k] = color;
        }

        switch(mHeader.AlphaDepth) {
            case 1: {
                auto colorIndex = 0u;
                for (auto k = 0u; k < (num_entries / 8u); ++k) {
                    const auto value = indices[k + num_entries];
                    for (auto j = 0u; j < 8; ++j, ++colorIndex) {
                        auto& color = layer_data[colorIndex];
                        color &= 0x00FFFFFFu;
                        color |= ALPHA_LOOKUP1[(((value & (1u << j))) != 0) ? 1 : 0] << 24u;
                    }
                }

                if ((num_entries % 8) != 0) {
                    const auto value = indices[num_entries + num_entries / 8];
                    for (auto j = 0u; j < (num_entries % 8); ++j, ++colorIndex) {
                        auto& color = layer_data[colorIndex];
                        color &= 0x00FFFFFFu;
                        color |= ALPHA_LOOKUP1[(((value & (1u << j))) != 0) ? 1 : 0] << 24u;
                    }
                }

                break;
            }

            case 4: {
                auto colorIndex = 0u;
                for (auto k = 0u; k < (num_entries / 2u); ++k) {
                    const auto value = indices[k + num_entries];
                    const uint8_t alpha0 = ALPHA_LOOKUP4[value & 0x0Fu];
                    const uint8_t alpha1 = ALPHA_LOOKUP4[value >> 4u];
                    auto& color1 = layer_data[colorIndex++];
                    auto& color2 = layer_data[colorIndex++];
                    color1 = (color1 & 0x00FFFFFFu) | (alpha0 << 24u);
                    color2 = (color2 & 0x00FFFFFFu) | (alpha1 << 24u);
                }

                if ((num_entries % 2) != 0) {
                    const auto value = indices[num_entries + num_entries / 2];
                    const uint8_t alpha = ALPHA_LOOKUP4[value & 0x0Fu];
                    auto& color = layer_data[colorIndex];
                    color = (color & 0x00FFFFFFu) | (alpha << 24u);
                }

                break;
            }

            case 8: {
                for(auto k = 0u; k < num_entries; ++k) {
                    const auto value = indices[num_entries + k];
                    auto& color = layer_data[k];
                    color &= 0x00FFFFFFu;
                    color |= ((uint32_t) value) << 24u;
                }
            }

            break;
        }

        glTexImage2D(GL_TEXTURE_2D, i, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, layer_data.data());
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glBindTexture(GL_TEXTURE_2D, 0);

    file->close();
}
