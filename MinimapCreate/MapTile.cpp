#include "stdafx.h"
#include "MapTile.h"
#include "GxDevice.h"
#include "GlExt.h"

static const float TileSize = 533.0f + 1.0f / 3.0f;
static const float ChunkSize = TileSize / 16.0f;
static const float UnitSize = ChunkSize / 8.0f;

MapTile::MapTile(FilePtr file, FileHandler handler, GxDevicePtr device) {
	mFile = file;
	mDevice = device;
	mHandler = handler;

	file->setPosition(0);

	try {
		while (true) {
			Chunk c;
			file->read(&c.signature, sizeof(c.signature));
			file->read(&c.size, sizeof(c.size));
			c.dataPos = file->getPosition();
			file->setPosition(c.dataPos + c.size);

			mChunks[c.signature] = c;
		}
	} catch (std::exception&) {

	}

	loadTextures();
	loadMcin();
	loadChunks();
}

MapTile::~MapTile() {

}

void MapTile::loadTextures() {
	auto itr = mChunks.find('MTEX');
	if (itr == mChunks.end()) {
	    return;
	}

	mFile->setPosition(itr->second.dataPos);
	std::vector<char> content(itr->second.size);
	mFile->read(content.data(), content.size());
	const char* ptr = content.data();
	unsigned int curOffset = 0;

	while (curOffset < content.size()) {
		auto file = mHandler->openFile(ptr);
		mTextures.push_back(std::make_shared<BlpTexture>(file, mDevice.get()));
		curOffset += strlen(ptr) + 1;
		ptr += strlen(ptr) + 1;
	}
}

void MapTile::loadMcin() {
	auto itr = mChunks.find('MCIN');
	if (itr == mChunks.end()) {
	    return;
	}

	mMcin.resize(256);
	mFile->setPosition(itr->second.dataPos);
	mFile->read(mMcin.data(), sizeof(MCIN) * 256);
}

void MapTile::loadChunks() {
    if(mMcin.empty()) {
        return;
    }

	for (int i = 0; i < 256; ++i) {
		MCIN& mcin = mMcin[i];
		auto cnk = std::make_shared<MapChunk>(mcin, mFile, this);
		mMapChunks.push_back(cnk);
	}
}

void MapTile::render(GxDevice* dev) {
	for (auto& cnk : mMapChunks) {
		cnk->render(dev);
	}
}

MapChunk::MapChunk(MCIN mcin, FilePtr file, MapTile* parent) {
	mParent = parent;
	mFile = file;
	mInfo = mcin;

	loadHeader();
	loadVertices();
	loadLayers();
}

MapChunk::~MapChunk() {
	glDeleteTextures(1, &mAlphaTex);
	glDeleteBuffers(1, &mVertexBuffer);
	glDeleteBuffers(1, &mIndexBuffer);
}

void MapChunk::loadChunk(unsigned int offset, uint32_t suggested_size) {
	if (offset == 0) {
		return;
	}

	mFile->setPosition(mInfo.mcnk + offset);
	Chunk c;
	mFile->read(&c.signature, sizeof(unsigned int));
	mFile->read(&c.size, sizeof(unsigned int));
	if(!c.size) {
	    c.size = suggested_size;
	}

	c.dataPos = mFile->getPosition();

	mChunks[c.signature] = c;
}

void MapChunk::loadHeader() {
	mFile->setPosition(mInfo.mcnk + 8);
	mFile->read(&mHeader, sizeof(MCNK));

	loadChunk(mHeader.mcvt);
	loadChunk(mHeader.mcnr);
	loadChunk(mHeader.mcal, mHeader.sizeAlpha - 8);
	loadChunk(mHeader.mcly);
	loadChunk(mHeader.mcrf);

	assert(mChunks.find('MCVT') != mChunks.end());
	assert(mChunks.find('MCNR') != mChunks.end());
	assert(mChunks.find('MCLY') != mChunks.end());
	assert(mChunks.find('MCAL') != mChunks.end());
}

void MapChunk::loadVertices() {
	auto itr = mChunks.find('MCVT'); // assert'ed before
	mFile->setPosition(itr->second.dataPos);

	float heights[145];
	mFile->read(heights, 145 * sizeof(float));

	itr = mChunks.find('MCNR');
	mFile->setPosition(itr->second.dataPos);
	signed char normals[145 * 3];
	mFile->read(normals, 3 * 145);

	mVertices.resize(145);
	int counter = 0;

	const float alpha_half = 0.5f * (62.0f / 64.0f) / 8.0f;

	for (int i = 0; i < 17; ++i) {
		for (int j = 0; j < (((i % 2) != 0) ? 8 : 9); ++j) {
			Vertex v;
			v.y = mHeader.IndexX * ChunkSize + j * UnitSize;
			if (i % 2)
				v.y += 0.5f * UnitSize;

			v.x = mHeader.IndexY * ChunkSize + i * UnitSize * 0.5f;
			v.z = mHeader.posz + heights[counter];
			v.nx = normals[counter * 3] / 127.0f;
			v.ny = normals[counter * 3 + 1] / 127.0f;
			v.nz = normals[counter * 3 + 2] / 127.0f;

			float tx = (float) j;
			float ty = i * 0.5f;
			if (i % 2)
				tx += 0.5;

			v.U = tx;
			v.V = ty;

			tx = (62.0f / 64.0f) / 8.0f * j;
			ty = (62.0f / 64.0f) / 8.0f * i * 0.5f;
			if (i % 2)
				tx += alpha_half;

			v.S = tx;
			v.T = ty;

			mVertices[counter] = v;
			++counter;
		}
	}
}

void MapChunk::initialize(GxDevice* dev) {

}

void MapChunk::loadLayers() {
	mLayers.resize(mHeader.NumLayers);
	auto itr = mChunks.find('MCLY');
	mFile->setPosition(itr->second.dataPos);
	mFile->read(mLayers.data(), sizeof(MCLY) * mLayers.size());

	itr = mChunks.find('MCAL');
	std::vector<unsigned char> alphaData(itr->second.size);
	mFile->setPosition(itr->second.dataPos);
	mFile->read(alphaData.data(), alphaData.size());

	mAlphaData.resize(4096);

	for (unsigned int i = 0; i < mHeader.NumLayers; ++i) {
		auto& ly = mLayers[i];
		if ((ly.flags & 0x100) == 0) {
			for (unsigned int j = 0; j < 4096; ++j) {
				mAlphaData[j] |= (0xFF << (i * 8));
			}
		} else if ((ly.flags & 0x200) == 0) {
			unsigned int mapPtr = ly.ofsMCAL;
			unsigned int bufferPtr = 0;

			for (int j = 0; j < 63; ++j) {
				for (int k = 0; k < 32; ++k) {
					unsigned char value = alphaData[mapPtr++];
					unsigned int alpha1 = (unsigned char) (255 * ((value & 0x0F) / (float) 0x0F));
					unsigned int alpha2 = (unsigned char) (255 * ((value & (k != 31 ? 0xF0 : 0x0F)) / (float) (k != 31 ? 0xF0 : 0x0F)));
					mAlphaData[bufferPtr++] |= (alpha1 << (i * 8));
					mAlphaData[bufferPtr++] |= (alpha2 << (i * 8));
				}
			}
		} else {
			unsigned int mapPtr = ly.ofsMCAL;
			unsigned int counterOut = 0;

			while (counterOut < 4096) {
				unsigned char curByte = alphaData[mapPtr];
				bool fill = (curByte & 0x80) != 0;
				unsigned int n = (curByte & 0x7F);
				++mapPtr;

				for (unsigned int k = 0; k < n; ++k) {
					mAlphaData[counterOut++] = alphaData[mapPtr] << (i * 8);
					if (fill == false) {
						++mapPtr;
					}
				}

				if (fill == true) {
					++mapPtr;
				}
			}
		}
	}
}

void MapChunk::render(GxDevice* dev) {
    static GLuint black_texture = -1;

    if(black_texture == -1) {
        glGenTextures(1, &black_texture);
        glBindTexture(GL_TEXTURE_2D, black_texture);
        uint8_t color = 0;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_R8, 1, 1, 0, GL_R8, GL_UNSIGNED_BYTE, &color);
    }

	if (!mLoaded) {
		unsigned short gIndices[768];

		for (unsigned int y = 0; y < 8; ++y) {
			for (unsigned int x = 0; x < 8; ++x) {
				uint32_t i = y * 8 * 12 + x * 12;

				gIndices[i + 0] = y * 17 + x;
				gIndices[i + 2] = y * 17 + x + 1;
				gIndices[i + 1] = y * 17 + x + 9;

				gIndices[i + 3] = y * 17 + x + 1;
				gIndices[i + 5] = y * 17 + x + 18;
				gIndices[i + 4] = y * 17 + x + 9;

				gIndices[i + 6] = y * 17 + x + 18;
				gIndices[i + 8] = y * 17 + x + 17;
				gIndices[i + 7] = y * 17 + x + 9;

				gIndices[i + 9] = y * 17 + x + 17;
				gIndices[i + 11] = y * 17 + x;
				gIndices[i + 10] = y * 17 + x + 9;
			}
		}

		glGenBuffers(1, &mVertexBuffer);
		glGenBuffers(1, &mIndexBuffer);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 768 * sizeof(short), gIndices, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, mVertices.size() * sizeof(Vertex), mVertices.data(), GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glGenTextures(1, &mAlphaTex);
		glBindTexture(GL_TEXTURE_2D, mAlphaTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 64, 64, 0, GL_RGBA, GL_UNSIGNED_BYTE, mAlphaData.data());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D, 0);

		mLoaded = true;
	}

	dev->setLayerShader(mHeader.NumLayers);

	auto prog = dev->getLayerShader(mHeader.NumLayers);

	if (mHeader.NumLayers > 1) {
		int locAlpha = glGetUniformLocation(prog, "_texAlpha");
		glActiveTexture(GL_TEXTURE0 + mHeader.NumLayers);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mAlphaTex);
		glUniform1i(locAlpha, mHeader.NumLayers);
	}

	for (unsigned int i = 0; i < mHeader.NumLayers; ++i) {
		std::stringstream strm;
		strm << "_texture" << i;
		int loc = glGetUniformLocation(prog, strm.str().c_str());
		glActiveTexture(GL_TEXTURE0 + i);
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, mParent->getTexture(mLayers[i].texture));
		glUniform1i(loc, i);
	}

	if(!mHeader.NumLayers) {
	    int loc = glGetUniformLocation(prog, "_texture0");
	    glActiveTexture(GL_TEXTURE0);
	    glEnable(GL_TEXTURE_2D);
	    glBindTexture(GL_TEXTURE_2D, black_texture);
	    glUniform1i(loc, 0);
	}

	glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);

	glEnableVertexAttribArray(glGetAttribLocation(prog, "position0"));
	glEnableVertexAttribArray(glGetAttribLocation(prog, "normal0"));
	glEnableVertexAttribArray(glGetAttribLocation(prog, "texcoord0"));

	glVertexAttribPointer(glGetAttribLocation(prog, "position0"), 3, GL_FLOAT, false, sizeof(Vertex), nullptr);
	glVertexAttribPointer(glGetAttribLocation(prog, "normal0"), 3, GL_FLOAT, false, sizeof(Vertex), (GLvoid*) 12);
	glVertexAttribPointer(glGetAttribLocation(prog, "texcoord0"), 2, GL_FLOAT, false, sizeof(Vertex), (GLvoid*) 24);
	if (mHeader.NumLayers > 1) {
		glEnableVertexAttribArray(glGetAttribLocation(prog, "texcoord1"));
		glVertexAttribPointer(glGetAttribLocation(prog, "texcoord1"), 2, GL_FLOAT, false, sizeof(Vertex), (GLvoid*)32);
	}

	glDrawElements(GL_TRIANGLES, 768, GL_UNSIGNED_SHORT, nullptr);
}