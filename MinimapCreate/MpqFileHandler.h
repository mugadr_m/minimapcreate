#pragma once

#include "IFileHandler.h"

class MpqFile : public IFile
{
	HANDLE mFile;
public:
	MpqFile(HANDLE hFile) {
		mFile = hFile;
	}

	void setPosition(int position) {
		if (SFileSetFilePointer(mFile, position, nullptr, FILE_BEGIN) != position) {
			throw std::invalid_argument("Invalid position specified");
		}
	}

	int getPosition() const {
		return SFileSetFilePointer(mFile, 0, nullptr, FILE_CURRENT);
	}

	void read(void* buffer, int numBytes) {
		DWORD numRead = 0;
		SFileReadFile(mFile, buffer, numBytes, &numRead, nullptr);
		if (numRead != numBytes) {
			throw std::invalid_argument("Unable to read bytes from file");
		}
	}

	void close() {
		SFileCloseFile(mFile);
	}
};

class MpqFileHandler : public IFileHandler
{
	struct ArchiveEntry
	{
		std::string path;
		HANDLE archive;
	};

	std::list<ArchiveEntry> mArchives;

public:
	MpqFileHandler(const std::string& basePath);

	FilePtr openFile(const std::string& fileName);
	bool hasFile(const std::string& fileName);
};