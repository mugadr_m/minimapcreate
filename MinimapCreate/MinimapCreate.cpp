#include "stdafx.h"
#define MMC_EXPORT
#include "MinimapCreate.h"
#include "MinimapCreator.h"
#include "MpqFileHandler.h"

MMC_DLL_EXPORT std::shared_ptr<IMinimapCreate> minimapCreateInit() {
	ULONG_PTR token;
	Gdiplus::GdiplusStartupInput inp;
	Gdiplus::GdiplusStartup(&token, &inp, nullptr);

	return std::make_shared<MinimapCreator>();
}

MMC_DLL_EXPORT FileHandler minimapCreateNewMpqHandler(const std::string& path) {
	return std::make_shared<MpqFileHandler>(path);
}

MMC_DLL_EXPORT std::string utf16ToUtf8(const std::wstring& str) {
    static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;
    return conversion.to_bytes(str);
}
