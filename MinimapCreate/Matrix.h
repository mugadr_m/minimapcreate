#pragma once

class Vector3
{
public:
	Vector3() : X(0), Y(0), Z(0) {
	}

	Vector3(float x, float y, float z) : X(x), Y(y), Z(z) {
	}

	void normalize() {
		float len = length();
		X /= len;
		Y /= len;
		Z /= len;
	}

	Vector3 normalized() const {
		float len = length();
		return Vector3(X / len, Y / len, Z / len);
	}

	// takes the lesser value of this and other for each coordinate as new coordinate
	void takeMin(const Vector3& other) {
		if (other.X < X)
			X = other.X;
		if (other.Y < Y)
			Y = other.Y;
		if (other.Z < Z)
			Z = other.Z;
	}

	// takes the greater value of this and other for each coordinate as new coordinate
	void takeMax(const Vector3& other) {
		if (other.X > X)
			X = other.X;
		if (other.Y > Y)
			Y = other.Y;
		if (other.Z > Z)
			Z = other.Z;
	}

	float lengthSquared() const {
		return X * X + Y * Y + Z * Z;
	}

	float length() const {
		return sqrt(lengthSquared());
	}

	float dot(const Vector3& lhs) const {
		return (X * lhs.X + Y * lhs.Y + Z * lhs.Z);
	}

	Vector3 cross(const Vector3& lhs) const {
		return Vector3(
			Y * lhs.Z - Z * lhs.Y,
			Z * lhs.X - X * lhs.Z,
			X * lhs.Y - Y * lhs.X
			);
	}

	static Vector3 cross(const Vector3& lhs, const Vector3& rhs) {
		return lhs.cross(rhs);
	}

	Vector3 operator + (const Vector3& lhs) const {
		return Vector3(X + lhs.X, Y + lhs.Y, Z + lhs.Z);
	}

	Vector3& operator += (const Vector3& lhs) {
		X += lhs.X;
		Y += lhs.Y;
		Z += lhs.Z;

		return *this;
	}

	Vector3 operator - (const Vector3& lhs) const {
		return Vector3(X - lhs.X, Y - lhs.Y, Z - lhs.Z);
	}

	Vector3& operator -= (const Vector3& lhs) {
		X -= lhs.X;
		Y -= lhs.Y;
		Z -= lhs.Z;

		return *this;
	}

	Vector3 operator / (float val) const {
		return Vector3(X / val, Y / val, Z / val);
	}

	Vector3& operator /= (float val) {
		X /= val;
		Y /= val;
		Z /= val;

		return *this;
	}

	Vector3 operator * (float val) const {
		return Vector3(X * val, Y * val, Z * val);
	}

	Vector3& operator *= (float val) {
		X *= val;
		Y *= val;
		Z *= val;

		return *this;
	}

	operator const float* () const {
		return &X;
	}

	operator float* () {
		return &X;
	}

	float X, Y, Z;

	static Vector3 UnitX, UnitY, UnitZ;

	static Vector3 negate(const Vector3& v) {
		return Vector3(-v.X, -v.Y, -v.Z);
	}
};


class Matrix
{
public:
	Matrix();

	void setIdentity();

	void transpose();
	Matrix transposed() const;
	bool invert();
	float determinant();

	Matrix operator * (const Matrix& rhs) const;
	Vector3 operator * (const Vector3& rhs) const;

	operator const float* () const {
		return _m;
	}

	operator float* () {
		return _m;
	}

#pragma warning (push)
	// non standard extension, unnamed struct/union
#pragma warning (disable: 4201)
	union
	{
		float m[4][4];
		float _m[16];
		struct
		{
			float m11, m12, m13, m14;
			float m21, m22, m23, m24;
			float m31, m32, m33, m34;
			float m41, m42, m43, m44;
		};
	};
#pragma warning (pop)

	static Matrix multiply(const Matrix& lhs, const Matrix& rhs);


#pragma region Generating Functions

	static Matrix rotationCenter(float angle, const Vector3& axis, const Vector3& center);
	static Matrix rotation(float angle, const Vector3& axis);
	static Matrix rotationAxis(const Vector3& angles);
	static Matrix translation(float x, float y, float z);
	static Matrix translation(const Vector3& v);
	static Matrix scale(float x, float y, float z);
	static Matrix scale(const Vector3& scales) { return scale(scales.X, scales.Y, scales.Z); }
	static Matrix ortho(float left, float top, float right, float bottom, float zNear, float zFar);
	static Matrix perspective(float fovy, float aspect, float zNear, float zFar);
	static Matrix lookAt(const Vector3& eye, const Vector3& at, const Vector3& up);

#pragma endregion
};