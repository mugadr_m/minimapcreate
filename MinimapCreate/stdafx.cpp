#include "stdafx.h"

HMODULE gDllModule = nullptr;

std::wstring utf8ToUtf16(const std::string& str) {
	static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> conversion;
	return conversion.from_bytes(str);
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID reserved) {
	switch (fdwReason) {
	case DLL_PROCESS_ATTACH:
		gDllModule = hDll;
		break;
	}

	return TRUE;
}