#pragma once

#include "IMinimapCreate.h"
#include "GxDevice.h"
#include "IFileHandler.h"

class MinimapCreator : public IMinimapCreate
{
	GxDevicePtr mDevice;
	FileHandler mFileHandler;
public:
	void initGraphics();
	void initFileSystem(FileHandler fileHandler);
	HBITMAP fromHandler(const std::string& tileName);
};