attribute vec4 position0;
attribute vec4 normal0;
attribute vec2 texcoord0;
attribute vec2 texcoord1;

uniform mat4 matView;
uniform mat4 matProj;

varying vec3 vertexNormal;
varying vec3 vertexPosition;
varying vec2 textureCoordinate0;
varying vec2 texCoordAlpha;

void main() {
	vertexNormal = normal0.xyz;
	vertexPosition = position0.xyz;
	textureCoordinate0 = texcoord0;
	texCoordAlpha = texcoord1;

	vec4 tmpPos = vec4(position0.xyz, 1.0);
	gl_Position = matProj * matView * tmpPos;
}