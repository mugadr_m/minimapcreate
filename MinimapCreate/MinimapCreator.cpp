#include "stdafx.h"
#include "MinimapCreator.h"
#include "MapTile.h"

void MinimapCreator::initFileSystem(FileHandler handler) {
	mFileHandler = handler;
}

void MinimapCreator::initGraphics() {
	mDevice = std::make_shared<GxDevice>();
}

HBITMAP MinimapCreator::fromHandler(const std::string& tileName) {
	auto file = mFileHandler->openFile(tileName);
	if(!file) {
	    return nullptr;
	}

	MapTile mt(file, mFileHandler, mDevice);

	auto res = mDevice->render(&mt);
	file->close();

	return res;
}